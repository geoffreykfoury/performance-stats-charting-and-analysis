# Performance Stats - Charting and Analysis#


Prototype created to explore options of charting data - not built for concurrency, needs refactoring.
Realtime production statistics and graphing application.  All sensitive data has been replaced with arbitrary values.


Analyzes and builds human readable visual representation of transaction occurances, types, time, size, and errors.
Pulling tens of thousands of records from an Oracle DB per day and analyzing them for averages, medians, duration, etc.

Calculates data by any selectable hour/day or day-to-day timeframe and builds charts and tables accordingly.

Quick Demo Video: [vimeo video link](https://vimeo.com/229005218)

### The Stack: ###

**FrontEnd**
	JQuery, Thymeleaf, Bootstrap, AJAX, Javascript, Select2, momentJS
	
**BackEnd**
	Java, JFreeChart, Spring Framework (Spring Sec, Spring Boot, etc), JDBC

**Server** 
 	Apache Tomcat 7
	
**DB**
	PLSQL, Oracle 12
	
**Host**
	AWS EC2 with Apache Tomcat, Oracle RDS

**---------**

![Query](https://i.imgur.com/Zkc2GMX.jpg)

![Query](https://i.imgur.com/eXQqH8L.jpg)


	


	