
$(document).ready(function () {


    $(function () {
        image();
    });

    function image() {
        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + '/Charts/image',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $.each(data, function (index, data) {
                var link = $("#" + (index + 1));
                link.attr("href", data);
                link.after("<div class='caption'> <div class='caption-content'><a style='color:inherit;' target='_blank' href='" + contextRoot + "/Charts/print/" + index + "'><i class='glyphicon glyphicon-print'></i></a></div></div>");
            });
        });
    }


    $(document).on('click', '#submit', function (e) {

        var months = {'Jan': '01', "Feb": '02', "Mar": '03', "Apr": '04', "May": '05', "Jun": '06', "Jul": '07', "Aug": '08', "Sep": '09', "Oct": '10', "Nov": '11', "Dec": '12'};
        var picker = $('.date').val();
        var newPicker = picker.split(" ");
        var date = newPicker[2] + "-" + months[newPicker[1]] + "-" + newPicker[3];

//        var url = $(location).attr('href');
//        var lastDate =href.substr(url.lasIndexOf('/')+1));
//        if(url.lasIndexOf('/')+1)
        window.location.href = contextRoot + "/Charts/" + date;

//        "/Charts/" 
    });


    $('#range').click(function () {
        var range = $('#right-nav');

        if ($('#range').is(':checked')) {
            range.append("<li style='padding-top:14px; padding-left:5px;' class='range'><input  placeholder='Start Date' class='date-st' type='text' id='start'/></li>");
            range.append("<li style='padding-top:14px; padding-left:5px;' class='range'><input  placeholder='End Date' class='date-en' type='text' id='end'/></li>");
            range.append("<li style='padding-top:12px; padding-left:5px;' class='range'><button  type='button' id='range-submit' class='btn btn-success btn-sm'>Submit</button></li>");
            setRangePicker();

        } else {
            $('.range').empty();
            $('.range').css("padding", '0px');

        }
    });

    $(document).on('click', '#range-submit', function (e) {

        var months = {'Jan': '01', "Feb": '02', "Mar": '03', "Apr": '04', "May": '05', "Jun": '06', "Jul": '07', "Aug": '08', "Sep": '09', "Oct": '10', "Nov": '11', "Dec": '12'};
        var pickerSt = $('.date-st').val();
        var newPicker = pickerSt.split(" ");
        var pickerEn = $('.date-en').val();
        var newPicker2 = pickerEn.split(" ");
        var range = newPicker[2] + "-" + months[newPicker[1]] + "-" + newPicker[3];
        var date = newPicker2[2] + "-" + months[newPicker2[1]] + "-" + newPicker2[3];

        window.open(contextRoot + "/Charts/range?date=" + date + "&range=" + range);

//        "/Charts/" 
    });



    $('#errors').on('click', function (e) {

        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + "/Charts/errors",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }

        }).success(function (data, status) {
            var numError = $('.error-line').length;
            if ($.isEmptyObject(data)) {
                $('#table-append').append("<tr class='error-line'><th>No Errors</th><td>No Errors</td></tr>");
            }
            if (numError > 0) {
                $('#table-append tbody').empty();
                $("#table-append").css("display", "none");
            } else {
                $.each(data, function (key, value) {
                    $('#table-append').append("<tr class='error-line'><th>" + key + "</th><td>" + value + "</td></tr>");
                });
                $("#table-append").css("display", "");
            }
        });
    });


    $('#top5').on('click', function (e) {

        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + "/Charts/top5",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }

        }).success(function (data, status) {
            var numError = $('.top-line').length;
            if ($.isEmptyObject(data)) {
                $('#tableTop-append').append("<tr class='top-line'><th>No Results, come back again soon.</th></tr>");
            }

            if (numError > 0) {
                $('#tableTop-append tbody').empty();
                $("#tableTop-append").css("display", "none");

            } else {

                $.each(data, function (i, val) {
                    $('#tableTop-append').append("<tr class='top-line'><th>" + val.system + "</th><td>" + val.sourceOp + "</td><td>" + val.rs + "</td><td>" + val.totalTime + "</td><td>" + val.bTime + "</td><td>" + val.aTime + "</td></tr>");

                });

                $("#tableTop-append").css("display", "");
            }
        });
    });

    $('#underX').on('click', function (e) {

        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + "/Charts/underX",
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $('#under-x').empty();
            $('#under-disclaimer').css("display", "none");
            
            var numUnderX = $('.under-x-line').length;
            if ($.isEmptyObject(data)) {
                $('#under-x-append').append("<tr class='error-line'><th>No Errors</th><td>No Errors</td></tr>");
            }


            if (numUnderX > 0) {
                $('#under-x-append tbody').empty();
                $("#under-x-append").css("display", "none");

            } else {
                var underArray = ["Over 5min", "Under 5min", "Under 1min", "Under 30sec", "Under 20sec", "Under 10sec", "Under 6sec", "Under 4sec", "Under 2sec", "Under 1sec"];

                $.each(data, function (index, value) {
                    if (++index > 10) {
                        return false;
                    }
                    $('#under-x-append').append("<tr class='under-x-line'><th>" + underArray[index-1] + "</th><td>" + value + "</td></tr>");
                });
                
                $('#under-disclaimer').css("display", "");
                $('#under-x').append("<img class='img-responsive' src='" + data[10] + "'/>");
                $("#under-x-append").css("display", "");
                $('html, body').animate({
                    scrollTop: $("#under-x-scroll").offset().top
                }, 1000);
            }
        });

    });


});

                            