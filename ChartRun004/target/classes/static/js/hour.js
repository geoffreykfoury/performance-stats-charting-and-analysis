/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {


    $(function () {
        imageHour();
    });

    function imageHour() {
        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + '/Charts/hourly/image',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $.each(data, function (index, data) {
                var link = $("#" + (index + 1));
                link.attr("href", data);
                link.after("<div class='caption'> <div class='caption-content'><a style='color:inherit;'  target='_blank' href='" + contextRoot + "/Charts/hourly/print/" + index +"'><i class='glyphicon glyphicon-print'></i></a></div></div>");
            });
        });
    }
    
    $('#advisory-code-submit').on('click', function (e) {
        
        var hour = $('#time-select').val();

        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + '/Charts/hourly/' + hour,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }

        }).success(function (data, status) {
            $('#error-results tbody').empty();
          
                $.each(data, function(index, data) {
                    $('#error-results ').append("<tr><td>" + data.code + "</td><td>" + data.displayTimestamp + "</td><td>" + data.id + "</td><td>" + data.system + "</td><td>" + data.op + "</td></tr><tr style='color:red;'><td></td><td></td><td>"
                            + data.message + "</td><td></td><td></td></tr>");
                                        
                });
                
                $("#advisory-code-div").css('display', '');
            });
            
      
    });
    
    $('#close-advisory-code').on('click', function(e){
        
        $("#advisory-code-div").css('display', 'none');
        
        
    });

});
