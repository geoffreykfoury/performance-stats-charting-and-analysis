/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {


    $(function () {
        imageRange();
    });

    function imageRange() {
        $.ajax({
            cache: false,
            type: 'GET',
            url: contextRoot + '/Charts/range/image',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {
            $.each(data, function (index, data) {
                var link = $("#" + (index + 1));
                link.attr("href", data);
                link.after("<div class='caption'> <div class='caption-content'><a style='color:inherit;' target='_blank' href='" + contextRoot + "/Charts/range/print/" + index +"'><i class='glyphicon glyphicon-print'></i></a></div></div>");
            });
        });
    }


});

