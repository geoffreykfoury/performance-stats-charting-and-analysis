package com.files.controllers;

import com.files.config.DateExtension;
import com.files.service.MainChartService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * DISCLAIMER = MANY OF THE METHODS FOR WRITING THIS WEB APP HAVE BEEN
 * COMPORISED PROGRMATICALLY TO ALLOW FOR CROSS BROWSWER SUPPORT MOST
 * SPECIFICALLY INTERNET EXPLORER (32K LIMIT ON DATA URI IN BASE64)
 *
 * THIS WEB APP IS RESPONSIVE
 */
@EnableConfigurationProperties(DateExtension.class)
@Controller
public class MainDayController {

    /** THIS IS THE EXTERNAL CONFIG FOR THE AMOUNT OF DAYS BACK ON THE CALANDER
    RESULTS CAN BE SELECTED FROM **/   
    @Autowired
    private DateExtension dateProperties;

    private final MainChartService run = new MainChartService();
    private int year;
    private int month;
    private int day;
    private int month1;
    private int day1;
    private int year1;
    private String date;
    private String selectedDate = date;
    private Calendar cal;

    List<String> trans = new ArrayList();

    @RequestMapping("/")
    public String homePage(Model model) {

        // GET YESTERDAY'S DATE ACCORDING TO THE SYSTEM CALENDAR (GREGORIAN ?!)
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        cal = Calendar.getInstance();

        cal.add(Calendar.DATE, -1);
        year1 = cal.get(Calendar.YEAR);
        month1 = cal.get(Calendar.MONTH);
        day1 = cal.get(Calendar.DAY_OF_MONTH);

        java.sql.Date yesterday = new java.sql.Date(cal.getTimeInMillis());
        date = dateFormat.format(yesterday);
        
       
        cal.add(Calendar.DATE, -this.dateProperties.getDateStart());
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);

        // RUN THE CHARTS AND PUT THEM INTO AN ARRAY LIST AS BASE64 STRINGS
        trans = run.runCharts(date);

        // RETURN THE ARRAYLIST AS MODEL FOR DYNAMIC TEMPLATING IN HTML
        model.addAttribute("all", trans);
        model.addAttribute("tDiff", run.getPrimeVsBackend());

        model.addAttribute("date", date);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("day", day);
        model.addAttribute("year1", year1);
        model.addAttribute("month1", month1);
        model.addAttribute("day1", day1);

        //  RETURN MAIN HTML PAGE WITH MODEL
        return "index";
    }

    // THIS METHOD IS NEEDED FOR CROSS-BROWSWER SUPPORT ****************
    @RequestMapping("/image")
    @ResponseBody
    public List<String> imageBuild() {
        return trans;
    }

    @RequestMapping("/print/{id}")
    public String print(Model model, @PathVariable int id) {

        String image = trans.get(id);
        model.addAttribute("image", image);

        return "print_page";
    }

    // THIS RETURNS THE NEW SELECTED DATE AND REPOPULATES THE INITIAL HOMEPAGE / INDEX
    @RequestMapping(value = "{date}")
    public String newDate(Model model, @PathVariable String date) {

        this.selectedDate = date;
        trans = run.runCharts(date);

        model.addAttribute("tDiff", run.getPrimeVsBackend());

        model.addAttribute("all", trans);
        model.addAttribute("year", year);
        model.addAttribute("month", month);
        model.addAttribute("day", day);
        model.addAttribute("year1", year1);
        model.addAttribute("month1", month1);
        model.addAttribute("day1", day1);
        return "index";
    }

    //GET THE ERRORS THAT OCCURED ON THIS DATE / DATES (USES THE SELECTED DATE)
    @RequestMapping(value = "/errors")
    @ResponseBody
    public Map errors() {
        return run.getErrors();
    }
    
    //GET THE TOP FIVE RESULTS IN SIZE
    @RequestMapping(value = "/top5")
    @ResponseBody
    public ArrayList top5() {
        return run.top5();
    }

    //ODDLY NAMED WEEK (CHANGED TO A RANGE SELECTION)...IT IS ACTUALLY A RANGE OF SELECTED DATES 
    // *******************PASSES BOTH DATES TO THE WEEK CONTROLLER
    @RequestMapping(value = "/week")
    public ModelAndView weekController() {

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        cal.add(Calendar.DATE, this.dateProperties.getDateStart());
        java.sql.Date weekly = new java.sql.Date(cal.getTimeInMillis());
        String week = dateFormat.format(weekly);

        return new ModelAndView("redirect:/range?date=" + date + "&range=" + week);

    }

     // *******************PASSES THE SELECTED DATE TO THE HOURLY CONTROLLER
    @RequestMapping(value = "/hour")
    public ModelAndView hourController() {

        return new ModelAndView("redirect:/hourly?date=" + selectedDate);

    }
    
    //***************** GET THE UNDER 'X' CHART *****************
    @RequestMapping (value ="/underX")
    @ResponseBody
    public ArrayList underX(){
        
        ArrayList underX = run.getTransUnderXTable();        
        return underX;
        
    }

}
