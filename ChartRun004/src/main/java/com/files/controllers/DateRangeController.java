/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.controllers;

import com.files.service.MainChartService;
import com.files.service.DateRangeChartService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author gkfoury
 */
@Controller
public class DateRangeController {

    private final DateRangeChartService run = new DateRangeChartService();
    List<String> transRange = new ArrayList();

    @RequestMapping("/range")
    public String rangeHome(Model model, @RequestParam("date") String date, @RequestParam("range") String range) {
        // RUN THE CHARTS AND PUT THEM INTO AN ARRAY LIST AS BASE64 STRINGS
        transRange = run.runCharts(date, range);

        model.addAttribute("all", transRange);
        model.addAttribute("tDiff", run.getPrimeVsBackend());

        return "rangeview";
    }

    @RequestMapping("/range/image")
    @ResponseBody
    public List<String> imageBuildRange() {
        return transRange;
    }

    @RequestMapping("/range/print/{id}")
    public String print(Model model, @PathVariable int id) {

        String image = transRange.get(id);
        model.addAttribute("image", image);

        return "print_page";
    }

}
