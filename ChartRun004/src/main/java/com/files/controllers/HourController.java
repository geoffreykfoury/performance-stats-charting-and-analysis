/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.controllers;

import com.files.models.ErrorAdvisoryCode;
import com.files.service.HourlyChartProcessor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author gkfoury
 */
@Controller
public class HourController {

    private ArrayList<String> hourLinks;
    private String averages;
    private String max;
    private String min;
    private String[] times = {"12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"};

    private HourlyChartProcessor hourly = new HourlyChartProcessor();

    @RequestMapping("/hourly")
    public String hourPage(Model model, @RequestParam("date") String date) {

        hourly.setDate(date);
        hourly.getAllHours();
        averages = hourly.getAvgAll();

        Map slots = hourly.getTableHourlyData();
        HashMap errors = hourly.getTableHourlyError();

        model.addAttribute("slots", slots);
        model.addAttribute("times", times);
        model.addAttribute("errors", errors);
        model.addAttribute("avg", averages);
        model.addAttribute("max", max);
        model.addAttribute("min", min);

        return "hour";

    }

    @RequestMapping("/hourly/image")
    @ResponseBody
    public ArrayList getImage() {

        hourLinks = new ArrayList();
        hourLinks.add(averages);
        hourLinks.add(max);
        hourLinks.add(min);

        return hourLinks;
    }

    @RequestMapping("/hourly/print/{id}")
    public String print(Model model, @PathVariable int id) {

        String image = hourLinks.get(id);
        model.addAttribute("image", image);

        return "print_page";
    }
    
     @ResponseBody
     @RequestMapping("/hourly/{hour}")
    public ArrayList print(@PathVariable int hour) {

        return hourly.getErrorAdvisoryCode(hour);
    }

}
