package com.files.DAO;

import com.files.DAO.MainDAO;
import com.files.models.AllTransactions;
import com.files.models.TransactionCalculated;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RangeDAO {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement sourceOp = null;

    private final String GET_SOURCESYSTEM_STATS = "SELECT SYSTEM, count(SYSTEM), MEDIAN(BOTTOM_LATENCY), MAX(BOTTOM_LATENCY), AVG(BOTTOM_LATENCY), AVG(TOP_LATENCY), MIN(BOTTOM_LATENCY) FROM SNACKS WHERE TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy')  AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy') GROUP BY SYSTEM";
//    private final String GET_WEEKLY_RS_ALL = "SELECT MEDIAN(PRIME_RS_SIZE), MAX(PRIME_RS_SIZE), AVG(PRIME_RS_SIZE), MIN(PRIME_RS_SIZE), MEDIAN(SOURCE_RS_SIZE), MAX(SOURCE_RS_SIZE), AVG(SOURCE_RS_SIZE), MIN(SOURCE_RS_SIZE) FROM SNACKS WHERE TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy')";
    private final String GET_SOURCE_COUNT = "SELECT OPERATION, COUNT(OPERATION) FROM SNACKS where SYSTEM = ? AND TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy') group by OPERATION";
    private final String GET_AVG_SOURCE_COUNT = "SELECT OPERATION, AVG(BOTTOM_LATENCY) FROM SNACKS where SYSTEM = ? AND TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy') group by OPERATION";
//    private final String GET_AVG_ALL_TRANS = "SELECT AVG(BOTTOM_LATENCY), AVG(TOP_LATENCY) FROM SNACKS where TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >= TO_DATE(?, 'dd-mm-yyyy')";
    private final String GET_HOURS_TRANS = "SELECT count(SYSTEM), EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy')  AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy') GROUP BY EXTRACT(hour FROM ACT_TS) ORDER BY EXTRACT(hour FROM ACT_TS)";
    private final String GET_HOUR_PER_SYSTEM = "SELECT count(SYSTEM), EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE SYSTEM = ? AND TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy') GROUP BY EXTRACT(hour FROM ACT_TS) ORDER BY EXTRACT(hour FROM ACT_TS)";
    private final String GET_ERROR_COUNT = "SELECT count(SYSTEM), OPERATION, SYSTEM FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') AND RESULT_CODE = 'ERROR' GROUP BY SYSTEM, OPERATION";
    private final String GET_ALL_SYSTEM = "SELECT MEDIAN(BOTTOM_LATENCY), MAX(BOTTOM_LATENCY), AVG(BOTTOM_LATENCY), AVG(TOP_LATENCY), MIN(BOTTOM_LATENCY) FROM SNACKS WHERE TRUNC(ACT_TS) <= TO_DATE(?, 'dd-mm-yyyy') AND TRUNC(ACT_TS) >=TO_DATE(?, 'dd-mm-yyyy')";

    public RangeDAO() {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            //This is my username and password for the database with the name 'XE'
            // This becomes VERY VERY particular making sure that the user has permissions / 'privileges' to the access the table / schema
//            String dbURL = "jdbc:oracle:thin:@localhost:1521:XE";
//            String username = "geoffrey2";
//            String password = "gfk";
            String dbURL = "jdbc:oracle:thin:@oracle-standard.cyusyczaeydg.us-east-1.rds.amazonaws.com:1521:ORCL";
            String username = "geoffrey";
            String password = "Mousehands";

            conn = DriverManager.getConnection(dbURL, username, password);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public HashMap getHoursByTrans(String date, String weekly) {
        HashMap<Integer, Integer> transByHour = new HashMap();
        try {
            sourceOp = conn.prepareStatement(GET_HOURS_TRANS);
            sourceOp.setString(1, date);
            sourceOp.setString(2, weekly);

            rs = sourceOp.executeQuery();

            while (rs.next()) {
                Integer hour = rs.getInt("EXTRACT(hourFROMACT_TS)");
                Integer count = rs.getInt("COUNT(SYSTEM)");
                transByHour.put(hour, count);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transByHour;
    }

    public HashMap getOpHour(String source, String date, String weekly) {
        HashMap<Integer, Integer> transByHour = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_HOUR_PER_SYSTEM);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            sourceOp.setString(3, weekly);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                Integer hour = rs.getInt("EXTRACT(HOURFROMACT_TS)");
                Integer count = rs.getInt("COUNT(SYSTEM)");
                transByHour.put(hour, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transByHour;
    }

//    public HashMap getMessageSize(String date, String weekly) {
//        HashMap<String, Integer> message = new HashMap();
//
//        try {
//
//            sourceOp = conn.prepareStatement(GET_WEEKLY_RS_ALL);
//            sourceOp.setString(1, date);
//            sourceOp.setString(2, weekly);
//            rs = sourceOp.executeQuery();
//
//            while (rs.next()) {
//                message.put("maxS", rs.getInt("MAX(SOURCE_RS_SIZE)"));
//                message.put("minS", rs.getInt("MIN(SOURCE_RS_SIZE)"));
//                message.put("medianS", rs.getInt("MEDIAN(SOURCE_RS_SIZE)"));
//                message.put("avgS", rs.getInt("AVG(SOURCE_RS_SIZE)"));
//                message.put("maxP", rs.getInt("MAX(PRIME_RS_SIZE)"));
//                message.put("minP", rs.getInt("MIN(PRIME_RS_SIZE)"));
//                message.put("medianP", rs.getInt("MEDIAN(PRIME_RS_SIZE)"));
//                message.put("avgP", rs.getInt("AVG(PRIME_RS_SIZE)"));
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                rs.close();
//                sourceOp.close();
//            } catch (Exception ignore) {
//            }
//        }
//        return message;
//    }
    public HashMap getSystemStats(String date, String weekly) {
        HashMap<String, TransactionCalculated> transactions = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_SOURCESYSTEM_STATS);
            sourceOp.setString(1, date);
            sourceOp.setString(2, weekly);
            rs = sourceOp.executeQuery();

            while (rs.next()) {

                TransactionCalculated trans = new TransactionCalculated();
                trans.setSystem(rs.getString("SYSTEM"));
                trans.setMax(rs.getInt("MAX(BOTTOM_LATENCY)"));
                trans.setMin(rs.getInt("MIN(BOTTOM_LATENCY)"));
                trans.setMedian(rs.getInt("MEDIAN(BOTTOM_LATENCY)"));
                trans.setAvgB(rs.getInt("AVG(BOTTOM_LATENCY)"));
                trans.setAvgSource(rs.getInt("AVG(TOP_LATENCY)"));
                trans.setCount(rs.getInt("COUNT(SYSTEM)"));

                trans.setMinSec(rs.getInt("MIN(BOTTOM_LATENCY)") / 1000.0);
                trans.setMaxSec(rs.getInt("MAX(BOTTOM_LATENCY)") / 1000.0);
                trans.setMedianSec(rs.getInt("MEDIAN(BOTTOM_LATENCY)") / 1000.0);
                trans.setAvgBSec(rs.getInt("AVG(BOTTOM_LATENCY)") / 1000.0);
                trans.setAvgA(rs.getInt("AVG(BOTTOM_LATENCY)") - rs.getInt("AVG(TOP_LATENCY)"));
                trans.setAvgASec(trans.getAvgA() / 1000.0);

                transactions.put(trans.getSystem(), trans);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transactions;
    }

    public HashMap getSourceCount(String source, String date, String weekly) {
        HashMap<String, Integer> sourceCount = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_SOURCE_COUNT);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            sourceOp.setString(3, weekly);

            rs = sourceOp.executeQuery();

            while (rs.next()) {
                String system = rs.getString("OPERATION");
                int count = rs.getInt("COUNT(OPERATION)");
                sourceCount.put(system, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return sourceCount;
    }

    public HashMap getAvgSourceCount(String source, String date, String weekly) {
        HashMap<String, Integer> avgOps = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_AVG_SOURCE_COUNT);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            sourceOp.setString(3, weekly);

            rs = sourceOp.executeQuery();

            while (rs.next()) {
                String system = rs.getString("OPERATION");
                int count = rs.getInt("AVG(BOTTOM_LATENCY)");
                avgOps.put(system, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return avgOps;
    }

    public AllTransactions getAvgSysTimeAll(String date, String weekly) {
        AllTransactions system = new AllTransactions();

        try {
            sourceOp = conn.prepareStatement(GET_ALL_SYSTEM);
            sourceOp.setString(1, date);
            sourceOp.setString(2, weekly);

            rs = sourceOp.executeQuery();

            while (rs.next()) {
                system.setMax(rs.getInt("MAX(BOTTOM_LATENCY)"));
                system.setMin(rs.getInt("MIN(BOTTOM_LATENCY)"));
                system.setMedian(rs.getInt("MEDIAN(BOTTOM_LATENCY)"));
                system.setAvgB(rs.getInt("AVG(BOTTOM_LATENCY)"));
                system.setAvgSource(rs.getInt("AVG(TOP_LATENCY)"));

                system.setMinSec(rs.getInt("MIN(BOTTOM_LATENCY)") / 1000.0);
                system.setMaxSec(rs.getInt("MAX(BOTTOM_LATENCY)") / 1000.0);
                system.setMedianSec(rs.getInt("MEDIAN(BOTTOM_LATENCY)") / 1000.0);

                system.setAvgBackSec(rs.getInt("AVG(TOP_LATENCY)") / 1000.0);
                system.setAvgA(rs.getInt("AVG(BOTTOM_LATENCY)") - rs.getInt("AVG(TOP_LATENCY)"));
                system.setAvgISec(rs.getInt("AVG(BOTTOM_LATENCY)") / 1000.0);

                system.setAvgSec(system.getAvgA() / 1000.0);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return system;
    }

}
