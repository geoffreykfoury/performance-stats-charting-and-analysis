package com.files.DAO;

import com.files.models.TransactionTimeSize;
import com.files.models.AllTransactions;
import com.files.models.ErrorAdvisoryCode;
import com.files.models.TransactionCalculated;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.lang.Integer;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;

public class MainDAO {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement sourceOp = null;

    // !!!!! GET THE HIGHEST RESULT SIZE (TOP 5)
    private final String GET_TOP5_SIZE = "select * from (select * from SNACKS order by MAIN_C_SIZE desc, SYSTEM) where rownum <=5 AND TRUNC(ACT_TS) =  TO_DATE(?, 'dd-mm-yyyy')";

    // !!!!! MAIN CHARTING STATEMENTS
    private final String GET_SOURCESYSTEM_STATS = "SELECT SYSTEM, count(SYSTEM), MEDIAN(BOTTOM_LATENCY), MAX(BOTTOM_LATENCY), AVG(BOTTOM_LATENCY), AVG(TOP_LATENCY), MIN(BOTTOM_LATENCY) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') GROUP BY SYSTEM";
    private final String GET_SOURCE_COUNT = "SELECT OPERATION, COUNT(OPERATION) FROM SNACKS where SYSTEM = ? AND TRUNC(ACT_TS) =  TO_DATE(?, 'dd-mm-yyyy') group by OPERATION";
    private final String GET_AVG_SOURCE_COUNT = "SELECT OPERATION, AVG(BOTTOM_LATENCY) FROM SNACKS where SYSTEM = ? AND TRUNC(ACT_TS) =  TO_DATE(?, 'dd-mm-yyyy') group by OPERATION";
    private final String GET_ALL_SYSTEM = "SELECT MEDIAN(BOTTOM_LATENCY), MAX(BOTTOM_LATENCY), AVG(BOTTOM_LATENCY), AVG(TOP_LATENCY), MIN(BOTTOM_LATENCY) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')";
    //  private final String GET_SOURCE_RS_ALL = "SELECT MEDIAN(MAIN_C_SIZE), MAX(MAIN_C_SIZE), AVG(MAIN_C_SIZE), MIN(MAIN_C_SIZE), MEDIAN(SOURCE_RS_SIZE), MAX(SOURCE_RS_SIZE), AVG(SOURCE_RS_SIZE), MIN(SOURCE_RS_SIZE) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')";
    private final String GET_HOURS_TRANS = "SELECT count(SYSTEM), EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') GROUP BY EXTRACT(hour FROM ACT_TS) ORDER BY EXTRACT(hour FROM ACT_TS)";
    private final String GET_HOUR_PER_SYSTEM = "SELECT count(SYSTEM), EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE SYSTEM = ? AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') GROUP BY EXTRACT(hour FROM ACT_TS) ORDER BY EXTRACT(hour FROM ACT_TS)";

    // !!!!! GET ALL ERRORS FOR SELECTED DAY 
    private final String GET_ERROR_COUNT = "SELECT count(SYSTEM), OPERATION, SYSTEM FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') AND RESULT_CODE = 'ERROR' GROUP BY SYSTEM, OPERATION";

    // !!!!! GET TRANSACTIONS BY RESPONSE TIME UNDER 'X'
    private final String GET_SPECIFIC_TRANS_TIMES = "select count(BOTTOM_LATENCY), 1 as ORD from SNACKS where BOTTOM_LATENCY > 300000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 2 as ORD from SNACKS where BOTTOM_LATENCY < 300000 AND BOTTOM_LATENCY > 60000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 3 as ORD from SNACKS where BOTTOM_LATENCY < 60000 AND BOTTOM_LATENCY > 30000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 4 AS ORD from SNACKS where BOTTOM_LATENCY < 30000 AND BOTTOM_LATENCY > 20000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 5 AS ORD from SNACKS where BOTTOM_LATENCY < 20000 AND BOTTOM_LATENCY > 10000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 6 AS ORD from SNACKS where BOTTOM_LATENCY < 10000 AND BOTTOM_LATENCY > 6000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 7 AS ORD from SNACKS where BOTTOM_LATENCY < 6000 AND BOTTOM_LATENCY > 4000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 8 AS ORD from SNACKS where BOTTOM_LATENCY < 4000 AND BOTTOM_LATENCY > 2000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 9 AS ORD from SNACKS where BOTTOM_LATENCY < 2000 AND BOTTOM_LATENCY > 1000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "union select count(BOTTOM_LATENCY), 10 AS ORD from SNACKS where BOTTOM_LATENCY < 1000 AND TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy')\n"
            + "ORDER BY ORD";

    public MainDAO() {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

          String dbURL = "jdbc:oracle:thin:@oracle-standard.cyusyczaeydg.us-east-1.rds.amazonaws.com:1521:ORCL";
            String username = "geoffrey";
            String password = "Mousehands";
//            String dbURL = "jdbc:oracle:thin:@localhost:1521:XE";
//            String username = "geoffrey2";
//            String password = "gfk";
            conn = DriverManager.getConnection(dbURL, username, password);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//***************** COMMAND TO GET SPECIFIC TRANSACTION TIMES UNDER 'X' *********************************************

    public ArrayList getSpecificTransTime(String date) {
        ArrayList<Integer> specificTransTimes = new ArrayList();

        try {
            sourceOp = conn.prepareStatement(GET_SPECIFIC_TRANS_TIMES);

            //need to create individual params for each prepared statement 
            //outer table does not work because of created column ORD and the 
            //inability to GROUP BY
            sourceOp.setString(1, date);
            sourceOp.setString(2, date);
            sourceOp.setString(3, date);
            sourceOp.setString(4, date);
            sourceOp.setString(5, date);
            sourceOp.setString(6, date);
            sourceOp.setString(7, date);
            sourceOp.setString(8, date);
            sourceOp.setString(9, date);
            sourceOp.setString(10, date);

            rs = sourceOp.executeQuery();

            while (rs.next()) {
                Integer transTime = rs.getInt("COUNT(BOTTOM_LATENCY)");

                specificTransTimes.add(transTime);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return specificTransTimes;
    }

    //*****************COMMAND TO GET THE TOP5 LARGEST TRANSACTIONS *********************************************
    //*************** COUNTING DOWN FROM FIVE TO PLACE IN MAP DESCENDING ORDER **********************************
    public ArrayList getTop5(String date) {

        ArrayList<TransactionTimeSize> top5 = new ArrayList();

        try {
            sourceOp = conn.prepareStatement(GET_TOP5_SIZE);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {

                TransactionTimeSize trans = new TransactionTimeSize();

                trans.setSystem(rs.getString("SYSTEM"));
                trans.setSourceOp(rs.getString("OPERATION"));

                trans.setRS(rs.getInt("MAIN_C_SIZE"));
                trans.setbTime(rs.getInt("TOP_LATENCY"));
                trans.setTotalTime(rs.getInt("BOTTOM_LATENCY"));
                trans.setaTime(rs.getInt("BOTTOM_LATENCY") - rs.getInt("TOP_LATENCY"));

                top5.add(trans);

            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return top5;
    }

//********************************** COMMAND TO GET THE TRANSACTIONS PER HOUR *******************************************
    public HashMap getHoursByTrans(String date) {
        HashMap<Integer, Integer> transByHour = new HashMap();
        try {
            sourceOp = conn.prepareStatement(GET_HOURS_TRANS);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                Integer hour = rs.getInt("EXTRACT(hourFROMACT_TS)");
                Integer count = rs.getInt("COUNT(SYSTEM)");
                transByHour.put(hour, count);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transByHour;
    }
//*************************************************************************************

    public HashMap getOpHour(String source, String date) {
        HashMap<Integer, Integer> transByHour = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_HOUR_PER_SYSTEM);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                Integer hour = rs.getInt("EXTRACT(HOURFROMACT_TS)");
                Integer count = rs.getInt("COUNT(SYSTEM)");
                transByHour.put(hour, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transByHour;
    }
//**********************************************************************************************************

    public HashMap getSystemStats(String date) {
        HashMap<String, TransactionCalculated> transactions = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_SOURCESYSTEM_STATS);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {

                TransactionCalculated trans = new TransactionCalculated();
                if (rs.getString("SYSTEM").equals("J_FAUX")) {
                    trans.setSystem("J_FAUX");
                } else {
                    trans.setSystem(rs.getString("SYSTEM"));
                }
                trans.setMax(rs.getInt("MAX(BOTTOM_LATENCY)"));
                trans.setMin(rs.getInt("MIN(BOTTOM_LATENCY)"));
                trans.setMedian(rs.getInt("MEDIAN(BOTTOM_LATENCY)"));
                trans.setAvgB(rs.getInt("AVG(BOTTOM_LATENCY)"));

                trans.setAvgSource(rs.getInt("AVG(TOP_LATENCY)"));
                trans.setCount(rs.getInt("COUNT(SYSTEM)"));

                trans.setMinSec(rs.getInt("MIN(BOTTOM_LATENCY)") / 1000.0);
                trans.setMaxSec(rs.getInt("MAX(BOTTOM_LATENCY)") / 1000.0);
                trans.setMedianSec(rs.getInt("MEDIAN(BOTTOM_LATENCY)") / 1000.0);

                trans.setAvgBSec(rs.getInt("AVG(TOP_LATENCY)") / 1000.0);
                trans.setAvgA(rs.getInt("AVG(BOTTOM_LATENCY)") - rs.getInt("AVG(TOP_LATENCY)"));
                trans.setAvgInterSec(rs.getInt("AVG(BOTTOM_LATENCY)") / 1000.0);

                trans.setAvgASec(trans.getAvgA() / 1000.0);

                transactions.put(trans.getSystem(), trans);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return transactions;
    }

    //**********************************************************************************************************
//    public HashMap getMessageSize(String date) {
//        HashMap<String, Integer> message = new HashMap();
//
//        try {
//
//            sourceOp = conn.prepareStatement(GET_SOURCE_RS_ALL);
//            sourceOp.setString(1, date);
//            rs = sourceOp.executeQuery();
//
//            while (rs.next()) {
//                message.put("maxS", rs.getInt("MAX(SOURCE_RS_SIZE)"));
//                message.put("minS", rs.getInt("MIN(SOURCE_RS_SIZE)"));
//                message.put("medianS", rs.getInt("MEDIAN(SOURCE_RS_SIZE)"));
//                message.put("avgS", rs.getInt("AVG(SOURCE_RS_SIZE)"));
//                message.put("maxP", rs.getInt("MAX(MAIN_C_SIZE)"));
//                message.put("minP", rs.getInt("MIN(MAIN_C_SIZE)"));
//                message.put("medianP", rs.getInt("MEDIAN(MAIN_C_SIZE)"));
//                message.put("avgP", rs.getInt("AVG(MAIN_C_SIZE)"));
//            }
//
//        } catch (SQLException ex) {
//            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                rs.close();
//                sourceOp.close();
//            } catch (Exception ignore) {
//            }
//        }
//        return message;
//    }
    //**********************************************************************************************************
    public HashMap getSourceCount(String source, String date) {
        HashMap<String, Integer> sourceCount = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_SOURCE_COUNT);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                String system = rs.getString("OPERATION");
                int count = rs.getInt("COUNT(OPERATION)");
                sourceCount.put(system, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return sourceCount;
    }

    //**********************************************************************************************************
    public HashMap getAvgSourceCount(String source, String date) {
        HashMap<String, Integer> avgOps = new HashMap();

        try {
            sourceOp = conn.prepareStatement(GET_AVG_SOURCE_COUNT);
            sourceOp.setString(1, source);
            sourceOp.setString(2, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                String system = rs.getString("OPERATION");
                int count = rs.getInt("AVG(BOTTOM_LATENCY)");
                avgOps.put(system, count);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return avgOps;
    }
//**********************************************************************************************************

    public AllTransactions getAvgSysTimeAll(String date) {
        AllTransactions system = new AllTransactions();

        try {
            sourceOp = conn.prepareStatement(GET_ALL_SYSTEM);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                system.setMax(rs.getInt("MAX(BOTTOM_LATENCY)"));
                system.setMin(rs.getInt("MIN(BOTTOM_LATENCY)"));
                system.setMedian(rs.getInt("MEDIAN(BOTTOM_LATENCY)"));
                system.setAvgB(rs.getInt("AVG(BOTTOM_LATENCY)"));
                system.setAvgSource(rs.getInt("AVG(TOP_LATENCY)"));

                system.setMinSec(rs.getInt("MIN(BOTTOM_LATENCY)") / 1000.0);
                system.setMaxSec(rs.getInt("MAX(BOTTOM_LATENCY)") / 1000.0);
                system.setMedianSec(rs.getInt("MEDIAN(BOTTOM_LATENCY)") / 1000.0);

                system.setAvgBackSec(rs.getInt("AVG(TOP_LATENCY)") / 1000.0);
                system.setAvgA(rs.getInt("AVG(BOTTOM_LATENCY)") - rs.getInt("AVG(TOP_LATENCY)"));
                system.setAvgISec(rs.getInt("AVG(BOTTOM_LATENCY)") / 1000.0);

                system.setAvgSec(system.getAvgA() / 1000.0);

            }
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return system;
    }
//**********************************************************************************************************

    public HashMap getErrors(String date) {
        HashMap<String, Integer> errors = new HashMap();
        try {
            sourceOp = conn.prepareStatement(GET_ERROR_COUNT);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                String operation = rs.getString("OPERATION");
                String system = rs.getString("SYSTEM");
                Integer count = rs.getInt("COUNT(SYSTEM)");

                String fullName = system + " / " + operation;
                errors.put(fullName, count);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return errors;

    }
//**********************************************************************************************************

}
//**********************************************************************************************************
