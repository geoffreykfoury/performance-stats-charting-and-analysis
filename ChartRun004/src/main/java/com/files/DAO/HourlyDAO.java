/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.DAO;

import com.files.models.ErrorAdvisoryCode;
import com.files.models.HourSlot;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gkfoury
 */
public class HourlyDAO {

    Connection conn = null;
    ResultSet rs = null;
    PreparedStatement sourceOp = null;

    private final String GET_HOURLY_TRANS = "SELECT count(SYSTEM), AVG(BOTTOM_LATENCY), MAX(BOTTOM_LATENCY), MIN(BOTTOM_LATENCY), SYSTEM, EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') GROUP BY EXTRACT(hour FROM ACT_TS), SYSTEM ORDER BY EXTRACT(hour FROM ACT_TS)";
    private final String GET_HOURLY_ERRORS = "SELECT count(SYSTEM), SYSTEM, EXTRACT(hour FROM ACT_TS) FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') AND RESULT_CODE = 'ERROR' GROUP BY  EXTRACT(hour FROM ACT_TS), SYSTEM ORDER BY EXTRACT(hour FROM ACT_TS)";
    // !!!!! GET ERRORS FOR TIME OF DAY AND ADVISORY CODE
    private final String GET_ADVISORY_CODE_PER_HOUR = "SELECT EXTRACT(hour FROM ACT_TS), ACT_TS, ID, ADVISORY_CODE, OPERATION, SYSTEM FROM SNACKS WHERE TRUNC(ACT_TS) = TO_DATE(?, 'dd-mm-yyyy') AND RESULT_CODE = 'ERROR' AND EXTRACT(hour FROM ACT_TS) = ? GROUP BY EXTRACT(hour FROM ACT_TS), ACT_TS, ID, ADVISORY_CODE, SYSTEM, OPERATION ORDER BY ACT_TS";

    public HourlyDAO() {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            //This is my username and password for the database with the name 'XE'
            // This becomes VERY VERY particular making sure that the user has permissions / 'privileges' to the access the table / schema
//            String dbURL = "jdbc:oracle:thin:@localhost:1521:XE";
//            String username = "geoffrey2";
//            String password = "gfk";
            String dbURL = "jdbc:oracle:thin:@oracle-standard.cyusyczaeydg.us-east-1.rds.amazonaws.com:1521:ORCL";
            String username = "geoffrey";
            String password = "Mousehands";
            conn = DriverManager.getConnection(dbURL, username, password);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList getSlotTrans(String date) {
        ArrayList<HourSlot> slotArray = new ArrayList<HourSlot>();
        try {

            sourceOp = conn.prepareStatement(GET_HOURLY_TRANS);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                HourSlot slot = new HourSlot();

                if (rs.getString("SYSTEM").equals("UNK")) {
                    slot.setSystem("OTHER");
                } else {
                    slot.setSystem(rs.getString("SYSTEM"));
                }
                slot.setCount(rs.getInt("COUNT(SYSTEM)"));
                slot.setHour(rs.getInt("EXTRACT(HOURFROMACT_TS)"));
                slot.setMin(rs.getInt("MIN(BOTTOM_LATENCY)"));
                slot.setMax(rs.getInt("MAX(BOTTOM_LATENCY)"));
                slot.setAvg(rs.getInt("AVG(BOTTOM_LATENCY)"));
                slotArray.add(slot);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return slotArray;
    }

    public ArrayList<ErrorAdvisoryCode> getErrorsAdvisoryCode(String date, int hour) {
        ArrayList<ErrorAdvisoryCode> errorList = new ArrayList();
        try {
            sourceOp = conn.prepareStatement(GET_ADVISORY_CODE_PER_HOUR);
            sourceOp.setString(1, date);
            sourceOp.setInt(2, hour);
            rs = sourceOp.executeQuery();

            while (rs.next()) {
                ErrorAdvisoryCode newError = new ErrorAdvisoryCode();
                newError.setTs(rs.getTimestamp("ACT_TS"));
                newError.setCode(rs.getInt("ADVISORY_CODE"));
                newError.setId(rs.getString("ID"));
                newError.setOp(rs.getString("OPERATION"));
                newError.setSystem(rs.getString("SYSTEM"));

                errorList.add(newError);
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }
        return errorList;
    }

    public HashMap<String, HashMap<Integer, Integer>> getHourlyError(String date) {

        HashMap<String, HashMap<Integer, Integer>> totalErrors = new HashMap();

        try {

            sourceOp = conn.prepareStatement(GET_HOURLY_ERRORS);
            sourceOp.setString(1, date);
            rs = sourceOp.executeQuery();

            while (rs.next()) {

                if (!totalErrors.containsKey(rs.getString("SYSTEM"))) {
                    HashMap<Integer, Integer> types = new HashMap();
                    types.put(rs.getInt("EXTRACT(HOURFROMACT_TS)"), rs.getInt("COUNT(SYSTEM)"));
                    totalErrors.put(rs.getString("SYSTEM"), types);
                } else {
                    totalErrors.get(rs.getString("SYSTEM")).put(rs.getInt("EXTRACT(HOURFROMACT_TS)"), rs.getInt("COUNT(SYSTEM)"));

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(MainDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                sourceOp.close();
            } catch (Exception ignore) {
            }
        }

        return totalErrors;

    }

}
