package com.files.service;

import com.files.DAO.HourlyDAO;
import com.files.models.ErrorAdvisoryCode;
import com.files.models.HourSlot;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.ui.TextAnchor;

public class HourlyChartProcessor {
    
    private String date;
    private String[] times = {"12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"};
    
    private HourlyDAO hourly = new HourlyDAO();
    
    ArrayList<HourSlot> hourTrans;
    
    public void getAllHours() {
        this.hourTrans = hourly.getSlotTrans(date);
    }
    
    public Map getTableHourlyData() {
        
        HashMap<String, HashMap<Integer, Integer>> allHours = new HashMap();
        
        for (HourSlot key : hourTrans) {
            HashMap<Integer, Integer> types = new HashMap();
            if (!allHours.containsKey(key.getSystem())) {
                types.put(key.getHour(), key.getCount());
                allHours.put(key.getSystem(), types);
            } else {
                allHours.get(key.getSystem()).put(key.getHour(), key.getCount());
            }
        }
        
        Map<String, HashMap<Integer, Integer>> map = new TreeMap<String, HashMap<Integer, Integer>>(allHours);
        return map;
    }
    
    public HashMap getTableHourlyError() {
        
        HashMap<String, HashMap<Integer, Integer>> allErrors = hourly.getHourlyError(date);
        
        return allErrors;
    }
    
    public ArrayList<ErrorAdvisoryCode> getErrorAdvisoryCode(int hour) {
        
        ArrayList<ErrorAdvisoryCode> errorCodeList = new ArrayList();
        errorCodeList = hourly.getErrorsAdvisoryCode(date, hour);
        String message = "";
        
        for (ErrorAdvisoryCode er : errorCodeList) {
            switch (er.getCode()) {
                
                case 100:
                    message = "'User error' Random error fake message";
                    break;
                case 101:
                    message = "'User error' Invalid Random error fake message combination";
                    break;
                case 102:
                    message = "'User error' Insufficient criteria Random error fake message";
                    break;
                case 103:
                    message = "'User error' System reports request Random error fake message error";
                    break;
                case 104:
                    message = "'User error' Root Error Random error fake message";
                    break;
                case 105:
                    message = "'User error' Making up a random error";
                    break;
                case 200:
                    message = "'Service Reported Error' Truncated Random error fake message (data overflow)";
                    break;
                case 201:
                    message = "'Service Reported Error' Random error fake message (soap)";
                    break;
                case 202:
                    message = "'Service Reported Error' Another error that has caused an error";
                    break;
                case 203:
                    message = "'Service Reported Error' Yet another random error and free thinking";
                    break;
                case 400:
                    message = "'Warning Condition' Error for a freestyler error writer";
                    break;
                case 800:
                    message = "'Internal Error' IO Failure - random error";
                    break;
                case 801:
                    message = "'Internal Error' DB Failure";
                    break;
                case 802:
                    message = "'Internal Error' Form Validation failure";
                    break;
                case 803:
                    message = "'Internal Error' Unrecognized media content caused root Error";
                    break;
                case 804:
                    message = "'Internal Error' Registration Failure";
                    break;
                case 899:
                    message = "'Internal Error' Unexpected internal failure caused by weather";
                    break;
                default:
                    message = "No key for this error code";
                    break;
            }
            er.setMessage(message);
            String timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(er.getTs());
            er.setDisplayTimestamp(timestamp);
            
        }
        
        return errorCodeList;
        
    }
    
    public String getAvgAll() {
        
        String bar = "";
        
        HashMap<Integer, Integer> avgs = new HashMap();
        int finalAvg = 0;
        for (int i = 0; i < 24; i++) {
            int average = 0;
            int counter = 0;
            for (HourSlot key : hourTrans) {
                if (key.getHour() == i) {
                    average += key.getAvg() * key.getCount();
                    counter += key.getCount();
                }
            }
            if (average > 0) {
                finalAvg = average / counter;
            } else {
                finalAvg = 0;
            }
            avgs.put(i, finalAvg);
        }
        
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        for (Integer key : avgs.keySet()) {
            dataset.addValue(avgs.get(key), key, times[key]);
        }
        
        JFreeChart chart = ChartFactory.createBarChart("Average Response Time per/hour", "Response Times",
                "Response Time", dataset, PlotOrientation.VERTICAL, true, true, false);
        
        chart.setBorderPaint(Color.gray);
        chart.setBorderStroke(new BasicStroke(5.0f));
        chart.setBorderVisible(true);
        
        CategoryPlot plot = chart.getCategoryPlot();
        LogAxis yAxis = new LogAxis("Response Time (ms)");
        yAxis.setBase(10);
        plot.setRangeAxis(yAxis);
        yAxis.setTickUnit(new NumberTickUnit(1));
        yAxis.setMinorTickMarksVisible(true);
        yAxis.setAutoRange(true);
        plot.setRangeAxis(yAxis);
        
        plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 16));
        plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));
        
        Double maximum = (Double) DatasetUtilities.findMaximumRangeValue(dataset);
        plot.getRangeAxis().setLowerBound(100);
        plot.getRangeAxis().setUpperBound(maximum * 1.6);
        
        CategoryItemRenderer renderer = plot.getRenderer();
        CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {
            
            @Override
            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                return null;
            }
            
            @Override
            public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                DecimalFormat df = new DecimalFormat("#,###.#");
                
                double secs = number / 1000.0;
                
                toAppendTo.append(df.format(secs));
                toAppendTo.append("sec");
                
                return toAppendTo;
            }
            
            @Override
            
            public Number parse(String source, ParsePosition parsePosition) {
                return null;
            }
            
        }
        );
        
        renderer.setBaseItemLabelGenerator(generator);
        renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 16));
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));
        
        renderer.setItemLabelFont(new Font("SansSerif", Font.BOLD, 12));
        
        CategoryAxis catAxis = plot.getDomainAxis();
        catAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
        catAxis.setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));
        catAxis.setCategoryMargin(0);
        
        BarRenderer barRenderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
        barRenderer.setItemMargin(-3);
//      barRenderer.setMaximumBarWidth(.15);

        BufferedImage newChart = chart.createBufferedImage(1200, 750);
        
        byte[] chartBarImage = null;
        try {
            chartBarImage = ChartUtilities.encodeAsPNG(newChart);
            
        } catch (IOException ex) {
            Logger.getLogger(MainChartProcessor.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
        bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
        
        return bar;
        
    }

//    public String getMinMaxAll(String operation) {
//
//        String bar = "";
//
//        HashMap<Integer, Integer> result = new HashMap();
//        for (int i = 0; i < 24; i++) {
//            int limit = 0;
//
//            for (Slot key : hourTrans) {
//                if (operation == "max") {
//                    if (key.getHour() == i) {
//                        if (key.getMax() > limit) {
//                            limit = key.getMax();
//                        }
//                    }
//                }
//                if (operation == "min") {
//                    if (key.getHour() == i) {
//                        if (key.getMin() > limit) {
//                            limit = key.getMin();
//                        }
//                    }
//                }
//            }
//            result.put(i, limit);
//        }
//
//        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
//
//        for (Integer key : result.keySet()) {
//            dataset.addValue(result.get(key), key, key);
//        }
//
//        JFreeChart chart = ChartFactory.createBarChart("(" + operation + ") Response Time per/hour", "Hours (0AM - 23PM)",
//                "Response Time", dataset, PlotOrientation.VERTICAL, true, true, false);
//
//        chart.setBorderPaint(Color.gray);
//        chart.setBorderStroke(new BasicStroke(5.0f));
//        chart.setBorderVisible(true);
//
//        CategoryPlot plot = chart.getCategoryPlot();
//        LogAxis yAxis = new LogAxis("Response Time (ms)");
//        yAxis.setBase(10);
//        plot.setRangeAxis(yAxis);
//        yAxis.setTickUnit(new NumberTickUnit(1));
//        yAxis.setMinorTickMarksVisible(true);
//        yAxis.setAutoRange(true);
//        plot.setRangeAxis(yAxis);
//
//        plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 16));
//        plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));
//
//        Double maximum = (Double) DatasetUtilities.findMaximumRangeValue(dataset);
//        plot.getRangeAxis().setLowerBound(100);
//        plot.getRangeAxis().setUpperBound(maximum * 1.6);
//
//        CategoryItemRenderer renderer = plot.getRenderer();
//        CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {
//
//            @Override
//            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
//                return null;
//            }
//
//            @Override
//            public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
//                DecimalFormat df = new DecimalFormat("###.#");
//
//                double secs = number / 1000.0;
//
//                
//                    toAppendTo.append(df.format(secs));
//                    toAppendTo.append("sec");
//                
//                return toAppendTo;
//            }
//
//            @Override
//            public Number parse(String source, ParsePosition parsePosition) {
//                return null;
//            }
//
//        });
//
//        renderer.setBaseItemLabelGenerator(generator);
//        renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 16));
//        renderer.setBaseItemLabelsVisible(true);
//        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
//                ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));
//
//        renderer.setItemLabelFont(new Font("SansSerif", Font.BOLD, 12));
//
//        CategoryAxis catAxis = plot.getDomainAxis();
//        catAxis.setCategoryLabelPositions(CategoryLabelPositions.STANDARD);
//        catAxis.setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));
//        catAxis.setCategoryMargin(0);
//
//        BarRenderer barRenderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
//        barRenderer.setItemMargin(-3);
//        barRenderer.setMaximumBarWidth(.15);
//
//        BufferedImage newChart = chart.createBufferedImage(1200, 750);
//
//        byte[] chartBarImage = null;
//        try {
//            chartBarImage = ChartUtilities.encodeAsPNG(newChart);
//
//        } catch (IOException ex) {
//            Logger.getLogger(ChartProcessor.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//
//        bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
//
//        return bar;
//
//    }
    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
}
