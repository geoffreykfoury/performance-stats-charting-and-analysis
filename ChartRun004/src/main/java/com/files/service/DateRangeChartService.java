package com.files.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.stereotype.Service;

@Service
public class DateRangeChartService {

    private final DateRangeChartProcessor processor = new DateRangeChartProcessor();

    public List<String> runCharts(String date, String weekly) {

        processor.setDate(date);
        processor.setWeekly(weekly);
        processor.getSysStats();

        List<String> trans = new ArrayList();
        trans.add(processor.getSystemTrans());
//        trans.add("blank");
//        trans.add(processor.getStackedBarChartAll("prime"));
        trans.add(processor.getBarChartAll());
        trans.add(processor.getHourlyTransactions());

        trans.add(processor.getBarChart("A_FAUX"));
        trans.add(processor.getSourceCount("A_FAUX", "trans"));
        trans.add(processor.getSourceCount("A_FAUX", "average"));
        trans.add(processor.getHourlyOperation("A_FAUX"));

        trans.add(processor.getBarChart("B_FAUX"));
        trans.add(processor.getSourceCount("B_FAUX", "trans"));
        trans.add(processor.getSourceCount("B_FAUX", "average"));
        trans.add(processor.getHourlyOperation("B_FAUX"));

        trans.add(processor.getBarChart("C_FAUX"));
        trans.add(processor.getSourceCount("C_FAUX", "trans"));
        trans.add(processor.getSourceCount("C_FAUX", "average"));
        trans.add(processor.getHourlyOperation("C_FAUX"));

        trans.add(processor.getBarChart("D_FAUX"));
        trans.add(processor.getSourceCount("D_FAUX", "trans"));
        trans.add(processor.getSourceCount("D_FAUX", "average"));
        trans.add(processor.getHourlyOperation("D_FAUX"));

        trans.add(processor.getBarChart("E_FAUX"));
        trans.add(processor.getSourceCount("E_FAUX", "trans"));
        trans.add(processor.getSourceCount("E_FAUX", "average"));
        trans.add(processor.getHourlyOperation("E_FAUX"));

        trans.add(processor.getBarChart("F_FAUX"));
        trans.add(processor.getSourceCount("F_FAUX", "trans"));
        trans.add(processor.getSourceCount("F_FAUX", "average"));
        trans.add(processor.getHourlyOperation("F_FAUX"));

        trans.add(processor.getBarChart("G_FAUX"));
        trans.add(processor.getSourceCount("G_FAUX", "trans"));
        trans.add(processor.getSourceCount("G_FAUX", "average"));
        trans.add(processor.getHourlyOperation("G_FAUX"));

        trans.add(processor.getBarChart("H_FAUX"));
        trans.add(processor.getSourceCount("H_FAUX", "trans"));
        trans.add(processor.getSourceCount("H_FAUX", "average"));
        trans.add(processor.getHourlyOperation("H_FAUX"));

        trans.add(processor.getBarChart("I_FAUX"));
        trans.add(processor.getSourceCount("I_FAUX", "trans"));
        trans.add(processor.getSourceCount("I_FAUX", "average"));
        trans.add(processor.getHourlyOperation("I_FAUX"));

        trans.add(processor.getBarChart("J_FAUX"));
        trans.add(processor.getSourceCount("J_FAUX", "trans"));
        trans.add(processor.getSourceCount("J_FAUX", "average"));
        trans.add(processor.getHourlyOperation("J_FAUX"));

//        trans.add(processor.getBarChartMessage());
        return trans;

    }

    public Map getPrimeVsBackend() {
        HashMap<String, double[]> primeBackend = processor.getStackedTable();

        Map<String, double[]> map = new TreeMap<String, double[]>(primeBackend);
        return map;

    }

}
