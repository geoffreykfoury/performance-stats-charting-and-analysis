package com.files.service;

import com.files.DAO.RangeDAO;
import com.files.models.AllTransactions;
import com.files.models.TransactionCalculated;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.labels.CategoryItemLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.category.StackedBarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.TextAnchor;

public class DateRangeChartProcessor {

    private final RangeDAO dataRetrieve = new RangeDAO();
    private String emptyDataSet64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAABmCAIAAAAvaWjFAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAqSSURBVHhe7Z1LrhQ5EEV7IT3s1TBiLQxYCxJLQSyF3gEDxAAhIdGOsJ2OfzqrcKXViqMc1PM3fOOmsyD99P76nSSbkaZMtiNNmWxHmjLZjjRlsh1pymQ70pTJdqQpk+1IUybbkaZMtiNNmWxHmjLZjiWm/PXh7de//6HXtw//tjrGvz/eQO33T+3no8S43n1uTQDdUUOHevvjSyvVsX198/HX7LzIp/d0QG8iijMprTI7fv6OjQ/15Dgttih47MIH//Lxm5URHSRt40zdUH1tHSZZZ8r3P9tPTYUjDQPI7vvv76zEF2TuCXFHAPN0zFgCYCYgsWmCeSusgTsRJZj0SGfgEmbKMT5aVkxnBI/NiFA/i25WMHxwSjU9HbbeLWMQscAaebhlRLzElAXr7gR1iljoMCNhvjlOOgKgmk5zwYhN4M/bYA3ciSjBpLXqh9EArPDtw0c6PjQmvsG+PFQreNbM2SYLYvADY5ZC3Wi617ENjV/eCZd4lSmNGxTixpvJyatrjrOOgCuKGRvjuilP1Q8mbVWYY7a1tFnYGqEx9Y0O1Q5+BAlZsLdDNXgDt0lrgTShaoFBas55nSmxkOgF8rU2tlK2vhMdEZxOPHQAMzaGN+8Bb+BNRAkm7VUy97A0+DEyJfbl8zrBt5aflPUJYvCOay86O34eC0S/RoLEvM6UXC+eA1NKW9+Jjgf1EcMdg7HVQlnViMcs6AbWRJRg0qEVHXZsnL4p+QO04QaPupX2zj1csIPEWeZMSfqKqC5y0045RK9Yt6Op70xHQU1ej8eMjfGAKSt8IkowKakaa4HC5h5lyiPx5sKD4KHK3SYLZFKKqzBdlPocTXTKPd8pa9zq4l0sfac6akg+zNgYQV4r1xMfTEqrukTUB8qU/lYHnMQWrcsZPPxO2duLBdKqB3iVKdlTwFqnTqch4lxHDdlfjdgEZ8mLGsiNvBFMyqpq93dl/KPx7aZ0viCyhKoFziTF5yWmxAWMBdsRg/rMcFrEyY6F0nLoW79OtXhkbBo9r4A28CeiBJPyqjoCXc79pmwK0/hFQo0F4kLiUH3WmRLF7Rd1En2OU2S5EnG2I8JikPL1clULnCVPNPAmogSTYtUIvrYkcu1gSgBFHtcRUkWsooAloYw+S0yZJM+Qpky2I02ZbEeaMtmONGWyHWnKZDvSlMl2pCmT7UhTJtuRpky2I02ZbMciU/aDBXDJF6D19e64jFM/+gW3egVMRzDfsdZDA/3y+3ovrI0lGO9z2WEZ3YWW8AtXHQTTq6yltRMS8gU0RY7MRVbzmrPcxgpT8hMiJW0j6zVJVAJ1AmXWlMEhhnZ6gJTAvLW76OvgLQGjJQkmZ0GCVSPWeYggmMM32nlBFXIuspi3DqiOX93GClMyA1Fw8eqmrFvakenrppRegRFciVVfk7klsG3S7dJ4xJSzv+VIYREecJHVvFzAu1llSmuFeAdbK2dnzx41Za/lO5Yi8AEhSNKogrDHREEX5CFTTv6WI2FK5FDA+1nynRLXLJ4gBXfl2L43vm5KlPuY68QcKh82zhKAFq36zcCgC/CYKZXJYLHwoyfmlMhiXi7g/Sz6h05/XtAksYcdg+o1bco6OF7Mgm5WKrJvkAy9hAYapZTr/djtEpiytpe9hm9ox7FxOsucEzkU8H6WmbLSklRNFt/E3YgXd8qqL9mxoOXzO+WALaEDQbpfW/0upintYEjVWDsUtjvBE3NKZPXZX8sdLDZlYeQv/Lpz7DpW8sgOAVBNC6z7n/lOydEWNIOkzHUJgqFV+Hgtn6nhPPNNiRwKeD/rTUn8hOLahhv6cv81eEZlLrkDsFaN0Al84KJDOjXlXJcgGFZVR/N/y5EyI3Is4O0sMGVZodi3uJRMDhRL3KZS1tqG3P0ql2J3rJMylcuYNSVTpoyWgGiHPdBl3pR9RUQE15QzIp8JeDNLdkpcM4rItaig58ZlKstGUDexziWWsJSLEfgmMcrhspLBmukGrsOe6MI7YtVYY21JdAhMCcQii8ELWCLDu4v1j+8kuUiaMtmONGWyHWnKZDvSlMl2pCmT7UhTJtuRpky2I02ZbEeaMtmONGWyHQtNKV/s8vNUQa2sKhd7I0yrzNe1+kBGP9CgrzrvzItpHj92KeXsDfLpK+lrmsihTlaB3cN45DLpgLu8+C4sMmVdLV2nPLoCCo5TC1jrnEWgiKqaRXnmCqS/+Ccf7WydxV/TyWYJTHlVkxBrFZZuPB7WC+M5pvti/1XJW1hiSlRHSiZOoIkE0C6WuA1VhXnl5oMDMqUErWkMMmHK2fjbbxvSlq4pZ8d8nSndUG9nhSnxFuRGqeB5qqaaTADRyxK3oaq0slCCe6cj+rkpp+OHz3y/cTP9kCYBf8iUZki3s8KUXmKqasR5T++UmFGeG9C9NYBaneNzU07HXyeCvsdXCK/vI5pEWKsQ4iB8Xt4L2ysBN2CBKfGRdC0BxpOxiHVcWsd+yRudb0jn/uvQwvn4qfvbZ8d8F8Ykq9NxHrimpN3b5ZkSqMrD5c/1cl6/U7ZECgVpe9pMoEdg/8pBiUmJFcm5U+fj70EOz3l9p8d84U45aNYUfW/j5d8pu+hBAixxG6JKPKCxdhi9X3yoc1NOx09Gxh/LCJ75ntVE8mdNWYAq9f8YN7HClPVxphbPH2FBAixxG7KKSWklXmttJoYXzsbPgoT0f3333tsRn9REYq3C0m3alBjJ/9mULUNUoPqAIIoHCbDEbagqNGIdR/sPqF4hTp0w5Wz8IkgMoFymKZ/URGKtwtLNN2X5POZCGWXf21hkSgD3hnGJVAUJQHFZ36Ol1h1LQGiczpBVllvpNAvP41fT1S7BfvOwJpLnTdna92Am530JC02ZJI+Rpky2I02ZbEeaMtmONGWyHWnKZDvSlMl2pCmT7UhTJtuRpky2I02ZbMdKU9YDB/2y3h2PWnG6p9eql9RAPdkwBrzUuFOPIKj31PB2uJRHb5Bl5MbgyVOsMqU6mgAm6MmrhqAeqtYZVjgSr/Otqy41boD5jN94HIZj5dqUxLXqsE/yJGtMifuNd1gGk6p2tZraboWa+Mm/THipcQXumTIXWpN2IUPJe8Y1ZQGDl75PHmWFKXEjdHcOrOX7UwU315bslnhMNjN3O3xluWSuMQIl2FJWdcOJJZyZkgefPMkKU0IKTdsh2iINTHbbn5g5yFBth7NdMtUYIBsktCH3zzAc2+zPTUmDT55klSlN2wH+k47mlZujJ3vshY5LJhrLrZp24YYjm9+5KcU4yTPstlO2ZJPPoz0Utl3Nc8l5Y2LWijcUvX/8Nh0sTFP+GTb7Ttl70cT3HYs6w3XJXOPSRlytuzBct9q5Kcm2mjzLClO2xJPdiIH5U5sKf6yzxNe9zf/LhFcaW7cE+fqoDAd9xe8oGqbkwSdPssSULffcl8WLPW2YaZpXTKr9Dw6gjUbMFGxdYWPiP0J1HrQ3DIddyuWaUgVv33XJNItMCWDy0B88qRXMnFsrEl+HmvoiGDb2HrJHuRiqUkM9Bqzj00u4PE35JAtNmSSPkaZMtiNNmWxHmjLZjjRlsh1pymQ70pTJdqQpk+1IUybbkaZMtiNNmWxHmjLZjjRlshm/f/8H5mluGs1uIkoAAAAASUVORK5CYII=";

    private String date;
    private String weekly;
    private AllTransactions systemAll;
    private String[] times = {"12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm"};

    //**** CHART SIZE     
    int width = 1200;
    int height = 750;

    HashMap<String, TransactionCalculated> transactions;

    public void getSysStats() {
        this.transactions = dataRetrieve.getSystemStats(date, weekly);
        this.systemAll = dataRetrieve.getAvgSysTimeAll(date, weekly);

    }

    public String getSystemTrans() {

        DefaultPieDataset dataset = new DefaultPieDataset();

        //Assign data from DB to dataset
        for (String key : transactions.keySet()) {
            dataset.setValue(key, transactions.get(key).getCount());
        }
        boolean legend = true;
        boolean tooltips = true;
        boolean urls = false;

        JFreeChart chart = ChartFactory.createPieChart("Source Systems", dataset, legend, tooltips, urls);
        chart.setBorderPaint(Color.gray);
        chart.setBorderStroke(new BasicStroke(5.0f));
        chart.setBorderVisible(true);

        //**** USE THIS TO CUSTOMIZE STYLE OF PIE CHART
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({2})"));

        plot.setLabelFont(new Font("SansSerif", Font.BOLD, 14));

        BufferedImage newChart = chart.createBufferedImage(width, height);

        byte[] chartBarImage = null;
        try {
            chartBarImage = ChartUtilities.encodeAsPNG(newChart);
        } catch (IOException ex) {
            Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }

        String pie = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);

        return pie;
    }

    public String getHourlyOperation(String source) {

        HashMap<Integer, Integer> transByHour = dataRetrieve.getOpHour(source, date, weekly);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        //Assign data from DB to dataset
        for (Integer key : transByHour.keySet()) {
            dataset.addValue(transByHour.get(key), "Transactions", times[key]);
        }

        JFreeChart chart = ChartFactory.createLineChart(
                "Transactions by Hour", "Hour",
                "Transactions",
                dataset, PlotOrientation.VERTICAL,
                true, true, false);

        chart.setBorderPaint(Color.gray);
        chart.setBorderStroke(new BasicStroke(5.0f));
        chart.setBorderVisible(true);

        BufferedImage newChart = chart.createBufferedImage(width, height);

        byte[] chartLine = null;
        try {
            chartLine = ChartUtilities.encodeAsPNG(newChart);
        } catch (IOException ex) {
            Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }

        String chartLineTrans = "data:image/png;base64," + Base64.encodeBase64String(chartLine);

        return chartLineTrans;

    }

    public String getHourlyTransactions() {

        HashMap<Integer, Integer> transByHour = dataRetrieve.getHoursByTrans(date, weekly);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        //Assign data from DB to dataset
        for (Integer key : transByHour.keySet()) {
            dataset.addValue(transByHour.get(key), "Transactions", times[key]);
        }

        JFreeChart chart = ChartFactory.createLineChart(
                "Transactions by Hour", "Hour",
                "Transactions",
                dataset, PlotOrientation.VERTICAL,
                true, true, false);

        chart.setBorderPaint(Color.gray);
        chart.setBorderStroke(new BasicStroke(5.0f));
        chart.setBorderVisible(true);

        BufferedImage newChart = chart.createBufferedImage(width, height);

        byte[] chartLine = null;
        try {
            chartLine = ChartUtilities.encodeAsPNG(newChart);
        } catch (IOException ex) {
            Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }

        String chartLineTrans = "data:image/png;base64," + Base64.encodeBase64String(chartLine);

        return chartLineTrans;

    }


    public HashMap getStackedTable() {
        HashMap<String, double[]> mainBackend = new HashMap();

        for (String key : transactions.keySet()) {

            double interfaces = transactions.get(key).getAvgB();
            double main = transactions.get(key).getAvgA();
            double mainSec = transactions.get(key).getAvgASec();
            double backSec = transactions.get(key).getAvgBSec();
            double[] difference = {interfaces, main, backSec, mainSec};
            mainBackend.put(key, difference);
        }
        double interfaces = systemAll.getAvgISec();
        double mainSec = systemAll.getAvgSec();
        double backSec = systemAll.getAvgBackSec();

        double[] avgs = {interfaces, mainSec, backSec};
        mainBackend.put("All Transactions", avgs);

        return mainBackend;
    }

//    public String getPrimeVsBack(String system) {
//        String pie = emptyDataSet64;
//
//        DefaultPieDataset dataset = new DefaultPieDataset();
//        //Assign data from DB to dataset
//
//        if (transactions.containsKey(system)) {
//
//            dataset.setValue("Prime", transactions.get(system).getAvgA());
//            dataset.setValue("Backend", transactions.get(system).getAvgSource());
//
//            boolean legend = true;
//            boolean tooltips = true;
//            boolean urls = false;
//
//            JFreeChart chart = ChartFactory.createPieChart("Average Share of Response Time (" + system + ")", dataset, legend, tooltips, urls);
//            chart.setBorderPaint(Color.gray);
//            chart.setBorderStroke(new BasicStroke(5.0f));
//            chart.setBorderVisible(true);
//
//            //**** USE THIS TO CUSTOMIZE STYLE OF PIE CHART
//            PiePlot plot = (PiePlot) chart.getPlot();
//            plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({2})"));
//
//            plot.setLabelFont(new Font("SansSerif", Font.BOLD, 14));
//            plot.setCircular(true);
//            plot.setLabelGap(0.03);
//
//            BufferedImage newChart = chart.createBufferedImage(width, height);
//
//            byte[] chartBarImage = null;
//            try {
//                chartBarImage = ChartUtilities.encodeAsPNG(newChart);
//            } catch (IOException ex) {
//                Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            pie = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
//        }
//        return pie;
//    }

    public String getBarChartAll() {

        String bar = emptyDataSet64;

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        Integer min = systemAll.getMin();
        Integer max = systemAll.getMax();
        Integer median = systemAll.getMedian();
        Integer avg = systemAll.getAvgB();

        dataset.addValue(avg, avg, "Avg");
        dataset.addValue(median, median, "Median");
        dataset.addValue(min, min, "Min");
        dataset.addValue(max, max, "Max");

        JFreeChart chart = ChartFactory.createBarChart("Response Times - Whole System", "Response Times",
                "Response Time", dataset, PlotOrientation.VERTICAL, true, true, false);
        chart.setBorderPaint(Color.gray);
        chart.setBorderStroke(new BasicStroke(5.0f));
        chart.setBorderVisible(true);

        CategoryPlot plot = chart.getCategoryPlot();
        plot.getDomainAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 16));

        CategoryItemRenderer renderer = plot.getRenderer();
        CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {

            //Format Bar Label and convert ms to seconds
            @Override
            public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                return null;
            }

            @Override
            public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                DecimalFormat df = new DecimalFormat("#,###.#");

                double secs = number / 1000.0;

                toAppendTo.append(df.format(secs));
                toAppendTo.append(" sec");

                return toAppendTo;
            }

            @Override
            public Number parse(String source, ParsePosition parsePosition) {
                return null;
            }

        });

        renderer.setBaseItemLabelGenerator(generator);
        renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 14));
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.CENTER, TextAnchor.BASELINE_CENTER));

        LogAxis yAxis = new LogAxis("Transaction Time (ms)");

        yAxis.setBase(10);
        plot.setRangeAxis(yAxis);
        yAxis.setTickUnit(new NumberTickUnit(1));
        yAxis.setMinorTickMarksVisible(true);
        yAxis.setAutoRange(true);
        plot.setRangeAxis(yAxis);
        plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 14));
        plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 12));

        Double maximum = (Double) DatasetUtilities.findMaximumRangeValue(dataset);

        plot.getRangeAxis().setLowerBound(10);
        plot.getRangeAxis().setUpperBound(maximum * 1.6);

        BarRenderer barRenderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
        barRenderer.setItemMargin(-0.65);
        barRenderer.setMaximumBarWidth(.15);

        BufferedImage newChart = chart.createBufferedImage(width, height);

        byte[] chartBarImage = null;
        try {
            chartBarImage = ChartUtilities.encodeAsPNG(newChart);
        } catch (IOException ex) {
            Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
        }

        bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);

        return bar;
    }

    public String getBarChart(String system) {

        String bar = emptyDataSet64;

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        if (transactions.containsKey(system)) {
            Integer min = transactions.get(system).getMin();
            Integer max = transactions.get(system).getMax();
            Integer median = transactions.get(system).getMedian();
            Integer avg = transactions.get(system).getAvgB();

            dataset.addValue(min, min, "Min");
            dataset.addValue(max, max, "Max");
            dataset.addValue(median, median, "Median");
            dataset.addValue(avg, avg, "Avg");

            JFreeChart chart = ChartFactory.createBarChart("Response Times (milliseconds) - " + system, "Response Times",
                    "Response Time", dataset, PlotOrientation.VERTICAL, true, true, false);
            chart.setBorderPaint(Color.gray);
            chart.setBorderStroke(new BasicStroke(5.0f));
            chart.setBorderVisible(true);

            CategoryPlot plot = chart.getCategoryPlot();
            plot.getDomainAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 16));

            CategoryItemRenderer renderer = plot.getRenderer();
            CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {

                @Override
                public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                    return null;
                }

                @Override
                public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
                    DecimalFormat df = new DecimalFormat("#,###.#");

                    double secs = number / 1000.0;

                    toAppendTo.append(df.format(secs));
                    toAppendTo.append(" sec");

                    return toAppendTo;
                }

                @Override
                public Number parse(String source, ParsePosition parsePosition) {
                    return null;
                }

            });

            renderer.setBaseItemLabelGenerator(generator);
            renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 14));
            renderer.setBaseItemLabelsVisible(true);
            renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                    ItemLabelAnchor.CENTER, TextAnchor.BASELINE_CENTER));

            LogAxis yAxis = new LogAxis("Transaction Time (ms)");

            yAxis.setBase(10);
            plot.setRangeAxis(yAxis);
            yAxis.setTickUnit(new NumberTickUnit(1));
            yAxis.setMinorTickMarksVisible(true);
            yAxis.setAutoRange(true);
            plot.setRangeAxis(yAxis);
            plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 14));
            plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 12));

            Double maximum = (Double) DatasetUtilities.findMaximumRangeValue(dataset);

            plot.getRangeAxis().setLowerBound(10);
            plot.getRangeAxis().setUpperBound(maximum * 1.6);

            BarRenderer barRenderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
            barRenderer.setItemMargin(-0.65);
            barRenderer.setMaximumBarWidth(.15);

            BufferedImage newChart = chart.createBufferedImage(width, height);

            byte[] chartBarImage = null;
            try {
                chartBarImage = ChartUtilities.encodeAsPNG(newChart);
            } catch (IOException ex) {
                Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
            }

            bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
        }

        return bar;
    }


    public String getSourceCount(String system, String type) {

        String bar = emptyDataSet64;

        //**** CHART SIZE
        int widthOp = 1200;
        int heightOp = 750;
        String title = "Transactions Per Source Operations (" + system + ")";

        HashMap<String, Integer> opCount = new HashMap();

        LogAxis yAxis = null;

        if (type.equals("trans")) {
            opCount = dataRetrieve.getSourceCount(system, date, weekly);
            yAxis = new LogAxis("Number of Transactions (" + system + ")");
        }

        if (type.equals("average")) {
            opCount = dataRetrieve.getAvgSourceCount(system, date, weekly);
            title = "Source Operations - Average Response Time (" + system + ")";
            yAxis = new LogAxis("Transaction Time in MS (" + system + ")");
        }

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        if (!opCount.isEmpty()) {

            if (system.equals("H_FAUX")) {

                for (String key : opCount.keySet()) {
                    dataset.addValue(opCount.get(key), opCount.get(key), key);
                }
                JFreeChart chart = ChartFactory.createBarChart(title, "Operations",
                        "Operations", dataset, PlotOrientation.VERTICAL, true, true, false);

                chart.setBorderPaint(Color.gray);
                chart.setBorderStroke(new BasicStroke(5.0f));
                chart.setBorderVisible(true);

                CategoryPlot plot = chart.getCategoryPlot();
                plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 14));
                plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 12));

                CategoryItemRenderer renderer = plot.getRenderer();
                CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {

                    @Override
                    public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                        return null;
                    }

                    @Override
                    public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {

                        DecimalFormat df = new DecimalFormat("#,###.#");
                        if (type.equals("average")) {

                            double secs = number / 1000.0;

                            toAppendTo.append(df.format(secs));
                            toAppendTo.append(" sec");

                        } else {
                            toAppendTo.append(number);
                        }
                        return toAppendTo;
                    }

                    @Override
                    public Number parse(String source, ParsePosition parsePosition) {
                        return null;
                    }

                });

                renderer.setBaseItemLabelGenerator(generator);
                renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 16));
                renderer.setBaseItemLabelsVisible(true);
                renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                        ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));

                renderer.setItemLabelFont(new Font("SansSerif", Font.BOLD, 14));

                CategoryAxis catAxis = plot.getDomainAxis();
                catAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
                catAxis.setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));

                BufferedImage newChart = chart.createBufferedImage(widthOp, heightOp);
                byte[] chartBarImage = null;
                try {
                    chartBarImage = ChartUtilities.encodeAsPNG(newChart);
                } catch (IOException ex) {
                    Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
                }

                bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
            } else {

                for (String key : opCount.keySet()) {
                    dataset.addValue(opCount.get(key), opCount.get(key), key);
                }
                JFreeChart chart = ChartFactory.createBarChart(title, "Operations",
                        "Operations", dataset, PlotOrientation.VERTICAL, true, true, false);

                chart.setBorderPaint(Color.gray);
                chart.setBorderStroke(new BasicStroke(5.0f));
                chart.setBorderVisible(true);

                CategoryPlot plot = chart.getCategoryPlot();

                yAxis.setBase(10);
                plot.setRangeAxis(yAxis);
                yAxis.setTickUnit(new NumberTickUnit(1));
                yAxis.setMinorTickMarksVisible(true);
                yAxis.setAutoRange(true);
                plot.setRangeAxis(yAxis);

                plot.getRangeAxis().setLabelFont(new Font("SansSerif", Font.BOLD, 14));
                plot.getRangeAxis().setTickLabelFont(new Font("SansSerif", Font.BOLD, 12));

                Double maximum = (Double) DatasetUtilities.findMaximumRangeValue(dataset);
                plot.getRangeAxis().setLowerBound(10);
                plot.getRangeAxis().setUpperBound(maximum * 1.6);

                CategoryItemRenderer renderer = plot.getRenderer();
                CategoryItemLabelGenerator generator = new StandardCategoryItemLabelGenerator("{2}", new NumberFormat() {

                    @Override
                    public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
                        return null;
                    }

                    @Override
                    public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {

                        DecimalFormat df = new DecimalFormat("#,###.#");
                        if (type.equals("average")) {

                            double secs = number / 1000.0;

                            toAppendTo.append(df.format(secs));
                            toAppendTo.append(" sec");

                        } else {
                            toAppendTo.append(number);
                        }
                        return toAppendTo;
                    }

                    @Override
                    public Number parse(String source, ParsePosition parsePosition) {
                        return null;
                    }

                });

                renderer.setBaseItemLabelGenerator(generator);
                renderer.setBaseItemLabelFont(new Font("SansSerif", Font.BOLD, 16));
                renderer.setBaseItemLabelsVisible(true);
                renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                        ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER));

                renderer.setItemLabelFont(new Font("SansSerif", Font.BOLD, 14));

                CategoryAxis catAxis = plot.getDomainAxis();
                catAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
                catAxis.setTickLabelFont(new Font("SansSerif", Font.BOLD, 14));
                catAxis.setCategoryMargin(0);

                BarRenderer barRenderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
                barRenderer.setItemMargin(-3);
                barRenderer.setMaximumBarWidth(.15);

                BufferedImage newChart = chart.createBufferedImage(widthOp, heightOp);
                byte[] chartBarImage = null;
                try {
                    chartBarImage = ChartUtilities.encodeAsPNG(newChart);
                } catch (IOException ex) {
                    Logger.getLogger(MainChartProcessor.class.getName()).log(Level.SEVERE, null, ex);
                }

                bar = "data:image/png;base64," + Base64.encodeBase64String(chartBarImage);
            }
        }
        return bar;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @param weekly the weekly to set
     */
    public void setWeekly(String weekly) {
        this.weekly = weekly;
    }

}
