package com.files.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.stereotype.Service;

@Service
public class MainChartService {

    private final MainChartProcessor processor = new MainChartProcessor();

    // This allows for AJAX to also add the image to the 'LightBox'  without this method
    // the images are loaded too fast and cannot be included within the anchor tag.
    // AJAX/JQUERY can access this list after the DOM has loaded.
    public List<String> runCharts(String date) {

        List<String> trans = new ArrayList();
        processor.setDate(date);
        processor.getSysStats();

        trans.add(processor.getSystemTrans());
        trans.add(processor.getBarChartAll());
        trans.add(processor.getHourlyTransactions());

        trans.add(processor.getBarChart("A_FAUX"));
        trans.add(processor.getSourceCount("A_FAUX", "trans"));
        trans.add(processor.getSourceCount("A_FAUX", "average"));
        trans.add(processor.getHourlyOperation("A_FAUX"));

        trans.add(processor.getBarChart("B_FAUX"));
        trans.add(processor.getSourceCount("B_FAUX", "trans"));
        trans.add(processor.getSourceCount("B_FAUX", "average"));
        trans.add(processor.getHourlyOperation("B_FAUX"));

        trans.add(processor.getBarChart("C_FAUX"));
        trans.add(processor.getSourceCount("C_FAUX", "trans"));
        trans.add(processor.getSourceCount("C_FAUX", "average"));
        trans.add(processor.getHourlyOperation("C_FAUX"));

        trans.add(processor.getBarChart("D_FAUX"));
        trans.add(processor.getSourceCount("D_FAUX", "trans"));
        trans.add(processor.getSourceCount("D_FAUX", "average"));
        trans.add(processor.getHourlyOperation("D_FAUX"));

        trans.add(processor.getBarChart("E_FAUX"));
        trans.add(processor.getSourceCount("E_FAUX", "trans"));
        trans.add(processor.getSourceCount("E_FAUX", "average"));
        trans.add(processor.getHourlyOperation("E_FAUX"));

        trans.add(processor.getBarChart("F_FAUX"));
        trans.add(processor.getSourceCount("F_FAUX", "trans"));
        trans.add(processor.getSourceCount("F_FAUX", "average"));
        trans.add(processor.getHourlyOperation("F_FAUX"));

        trans.add(processor.getBarChart("G_FAUX"));
        trans.add(processor.getSourceCount("G_FAUX", "trans"));
        trans.add(processor.getSourceCount("G_FAUX", "average"));
        trans.add(processor.getHourlyOperation("G_FAUX"));

        trans.add(processor.getBarChart("H_FAUX"));
        trans.add(processor.getSourceCount("H_FAUX", "trans"));
        trans.add(processor.getSourceCount("H_FAUX", "average"));
        trans.add(processor.getHourlyOperation("H_FAUX"));

        trans.add(processor.getBarChart("I_FAUX"));
        trans.add(processor.getSourceCount("I_FAUX", "trans"));
        trans.add(processor.getSourceCount("I_FAUX", "average"));
        trans.add(processor.getHourlyOperation("I_FAUX"));

        trans.add(processor.getBarChart("J_FAUX"));
        trans.add(processor.getSourceCount("J_FAUX", "trans"));
        trans.add(processor.getSourceCount("J_FAUX", "average"));
        trans.add(processor.getHourlyOperation("J_FAUX"));

        return trans;

    }

    public Map getPrimeVsBackend() {
        HashMap<String, double[]> primeBackend = processor.getStackedTable();

        Map<String, double[]> map = new TreeMap<String, double[]>(primeBackend);
        return map;

    }

    public Map getErrors() {
        HashMap<String, Integer> errors = processor.getErrors();

        Map<String, Integer> map = new TreeMap<>(errors);

        return map;
    }

    public ArrayList top5() {
        ArrayList top5 = processor.getTop5();
        return top5;
    }

    public ArrayList getTransUnderXTable() {

        ArrayList underXTable = processor.getUnderXTable();
        String underX = processor.getTransUnderX();
        
        underXTable.add(underX);
        return underXTable;
    }

}
