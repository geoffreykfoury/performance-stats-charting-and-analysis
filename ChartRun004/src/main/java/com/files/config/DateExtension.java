/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 *
 * @author gkfoury
 */
@ConfigurationProperties("date")
public class DateExtension {
    
    private int dateStart = 30;

    /**
     * @return the dateStart
     */
    public int getDateStart() {
        return dateStart;
    }

    /**
     * @param dateStart the dateStart to set
     */
    public void setDateStart(int dateStart) {
        this.dateStart = dateStart;
    }
    
    
    
    
}
