/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.models;

import java.sql.Timestamp;

/**
 *
 * @author gkfoury
 */
public class ErrorAdvisoryCode {
    
    private Timestamp ts;
    private String id;
    private int code;
    private String op;
    private String system;
    private String message;
    private String displayTimestamp;

    /**
     * @return the ts
     */
    public Timestamp getTs() {
        return ts;
    }

    /**
     * @param ts the ts to set
     */
    public void setTs(Timestamp ts) {
        this.ts = ts;
    }


    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the sourceOp
     */
    public String getOp() {
        return op;
    }

    /**
     * @param op the sourceOp to set
     */
    public void setOp(String op) {
        this.op = op;
    }

    /**
     * @return the system
     */
    public String getSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the displayTimetamp
     */
    public String getDisplayTimestamp() {
        return displayTimestamp;
    }

    /**
     * @param displayTimetamp the displayTimetamp to set
     */
    public void setDisplayTimestamp(String displayTimetamp) {
        this.displayTimestamp = displayTimetamp;
    }
    
    
    
}
