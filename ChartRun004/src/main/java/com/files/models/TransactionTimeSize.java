/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.models;

/**
 *
 * @author gkfoury
 */
public class TransactionTimeSize {
    
    private String system;
    private String sourceOp;
    private int RS;
    private int totalTime;
    private int bTime;
    private int aTime;

    /**
     * @return the system
     */
    public String getSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return the sourceOp
     */
    public String getSourceOp() {
        return sourceOp;
    }

    /**
     * @param sourceOp the sourceOp to set
     */
    public void setSourceOp(String sourceOp) {
        this.sourceOp = sourceOp;
    }

    /**
     * @return the primeRS
     */
    public int getRS() {
        return RS;
    }

    /**
     * @param RS the primeRS to set
     */
    public void setRS(int RS) {
        this.RS = RS;
    }

    /**
     * @return the totalTime
     */
    public int getTotalTime() {
        return totalTime;
    }

    /**
     * @param totalTime the totalTime to set
     */
    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * @return the backendTime
     */
    public int getbTime() {
        return bTime;
    }

    /**
     * @param bTime the backendTime to set
     */
    public void setbTime(int bTime) {
        this.bTime = bTime;
    }

    /**
     * @return the primeTime
     */
    public int getaTime() {
        return aTime;
    }

    /**
     * @param aTime the primeTime to set
     */
    public void setaTime(int aTime) {
        this.aTime = aTime;
    }


    
    
    
    
}
