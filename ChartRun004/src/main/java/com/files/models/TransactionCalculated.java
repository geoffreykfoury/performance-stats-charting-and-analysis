
package com.files.models;

public class TransactionCalculated {

    private String system;
    private int count = 0;
    private int median = 0;
    private int max = 0;
    private int min = 0;
    private int avgSource = 0;
    private int avgB = 0;
    private int avgA = 0;


    
    //SECONDS
    private double avgInterSec =0;
    private double avgASec = 0;
    private double avgBSec = 0;
    private double medianSec = 0;
    private double minSec = 0;
    private double maxSec = 0;

    /**
     * @return the system
     */
    public String getSystem() {
        return system;
    }

    /**
     * @param system the system to set
     */
    public void setSystem(String system) {
        this.system = system;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the median
     */
    public int getMedian() {
        return median;
    }

    /**
     * @param median the median to set
     */
    public void setMedian(int median) {
        this.median = median;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * @return the avgSource
     */
    public int getAvgSource() {
        return avgSource;
    }

    /**
     * @param avgSource the avgSource to set
     */
    public void setAvgSource(int avgSource) {
        this.avgSource = avgSource;
    }

    /**
     * @return the avgInter
     */
    public int getAvgB() {
        return avgB;
    }

    /**
     * @param avgB the avgInter to set
     */
    public void setAvgB(int avgB) {
        this.avgB = avgB;
    }

    /**
     * @return the avgPrimeSec
     */
    public double getAvgASec() {
        return avgASec;
    }

    /**
     * @param avgASec the avgPrimeSec to set
     */
    public void setAvgASec(double avgASec) {
        this.avgASec = avgASec;
    }

    /**
     * @return the avgBackSec
     */
    public double getAvgBSec() {
        return avgBSec;
    }

    /**
     * @param avgBSec the avgBackSec to set
     */
    public void setAvgBSec(double avgBSec) {
        this.avgBSec = avgBSec;
    }

    /**
     * @return the medianSec
     */
    public double getMedianSec() {
        return medianSec;
    }

    /**
     * @param medianSec the medianSec to set
     */
    public void setMedianSec(double medianSec) {
        this.medianSec = medianSec;
    }

    /**
     * @return the minSec
     */
    public double getMinSec() {
        return minSec;
    }

    /**
     * @param minSec the minSec to set
     */
    public void setMinSec(double minSec) {
        this.minSec = minSec;
    }

    /**
     * @return the maxSec
     */
    public double getMaxSec() {
        return maxSec;
    }

    /**
     * @param maxSec the maxSec to set
     */
    public void setMaxSec(double maxSec) {
        this.maxSec = maxSec;
    }

    /**
     * @return the avgPrime
     */
    public int getAvgA() {
        return avgA;
    }

    /**
     * @param avgA the avgPrime to set
     */
    public void setAvgA(int avgA) {
        this.avgA = avgA;
    }

    /**
     * @return the avgInterSec
     */
    public double getAvgInterSec() {
        return avgInterSec;
    }

    /**
     * @param avgInterSec the avgInterSec to set
     */
    public void setAvgInterSec(double avgInterSec) {
        this.avgInterSec = avgInterSec;
    }

}