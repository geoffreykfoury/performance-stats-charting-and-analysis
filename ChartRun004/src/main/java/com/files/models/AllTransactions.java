/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.files.models;

/**
 *
 * @author gkfoury
 */
public class AllTransactions {

    private int median = 0;
    private int max = 0;
    private int min = 0;
    private int avgSource = 0;
    private int avgB = 0;
    private int avgA = 0;

    private double avgISec = 0;
    private double avgSec = 0;
    private double avgBackSec = 0;
    private double medianSec = 0;
    private double minSec = 0;
    private double maxSec = 0;

    /**
     * @return the median
     */
    public int getMedian() {
        return median;
    }

    /**
     * @param median the median to set
     */
    public void setMedian(int median) {
        this.median = median;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(int max) {
        this.max = max;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        this.min = min;
    }

    /**
     * @return the avgSource
     */
    public int getAvgSource() {
        return avgSource;
    }

    /**
     * @param avgSource the avgSource to set
     */
    public void setAvgSource(int avgSource) {
        this.avgSource = avgSource;
    }

    /**
     * @return the avgInter
     */
    public int getAvgB() {
        return avgB;
    }

    /**
     * @param avgB the avgInter to set
     */
    public void setAvgB(int avgB) {
        this.avgB = avgB;
    }

    /**
     * @return the avgPrime
     */
    public int getAvgA() {
        return avgA;
    }

    /**
     * @param avgA the avgPrime to set
     */
    public void setAvgA(int avgA) {
        this.avgA = avgA;
    }

    /**
     * @return the avgInterSec
     */
    public double getAvgISec() {
        return avgISec;
    }

    /**
     * @param avgISec the avgInterSec to set
     */
    public void setAvgISec(double avgISec) {
        this.avgISec = avgISec;
    }

    /**
     * @return the avgPrimeSec
     */
    public double getAvgSec() {
        return avgSec;
    }

    /**
     * @param avgSec the avgPrimeSec to set
     */
    public void setAvgSec(double avgSec) {
        this.avgSec = avgSec;
    }

    /**
     * @return the avgBackSec
     */
    public double getAvgBackSec() {
        return avgBackSec;
    }

    /**
     * @param avgBackSec the avgBackSec to set
     */
    public void setAvgBackSec(double avgBackSec) {
        this.avgBackSec = avgBackSec;
    }

    /**
     * @return the medianSec
     */
    public double getMedianSec() {
        return medianSec;
    }

    /**
     * @param medianSec the medianSec to set
     */
    public void setMedianSec(double medianSec) {
        this.medianSec = medianSec;
    }

    /**
     * @return the minSec
     */
    public double getMinSec() {
        return minSec;
    }

    /**
     * @param minSec the minSec to set
     */
    public void setMinSec(double minSec) {
        this.minSec = minSec;
    }

    /**
     * @return the maxSec
     */
    public double getMaxSec() {
        return maxSec;
    }

    /**
     * @param maxSec the maxSec to set
     */
    public void setMaxSec(double maxSec) {
        this.maxSec = maxSec;
    }

}
